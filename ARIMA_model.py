import pandas as pd
import numpy as np
import os
import requests
import io
import matplotlib.pylab as plt
import warnings
from statsmodels.tsa.stattools import adfuller

#Change directory to our main local computer documents
os.chdir('/home/jeff/Documents/COVID_19_time_series_predictions/')


#import pandas dataframe for cases in by provinces
url = 'https://raw.githubusercontent.com/ishaberry/Covid19Canada/master/timeseries_prov/cases_timeseries_prov.csv'
s=requests.get(url).content
df=pd.read_csv(io.StringIO(s.decode('utf-8')),parse_dates=True)
# df = pd.read_csv(r'../timeseries_prov/cases_timeseries_prov.csv')
#This is for the alberta only cases:
a_df = df[df['province'] == 'Alberta'][['date_report','cases']]

#This gives you an actual datetime index instead of a string
a_df['date_report'] = pd.to_datetime(a_df['date_report'], dayfirst=True)
a_df['weekend'] = a_df['date_report'].dt.dayofweek
# a_df['date_report'] = a_df['date_report'].dt.strftime('%m-%d-%Y')
# a_df['date_report'] = a_df['date_report'].apply(lambda x: x.strftime('%d-%m-%Y'))


#Set date index as your index. This should make things easier for plotting and such
a_df = a_df.set_index(['date_report'])
a_df.index = pd.to_datetime(a_df.index)

#Remove up until start of pandemic (April 3 really)
a_df = a_df.drop(pd.date_range('2020-01-25', '2020-03-05'), errors='ignore')

#Plot cases, plot rolling window
ax = a_df['cases'].plot(figsize=(12,5), title='Cases in alberta')
ax.set(xlabel = 'Date', ylabel= 'cases')
ax.autoscale(axis='both', tight=True)
a_df.rolling(window=7).mean()['cases'].plot

# expanding for mean in case
a_df['cases'].expanding().mean().plot(figsize=(12,5))

#here, we will fille the weekend 0 numbers with the average from the monday or tuesday depending on holidays
for k in list(np.where((a_df['weekend'] == 0) | (a_df['weekend'] == 1)))[0][3:]:
    if a_df['cases'].iloc[k - 4:k].sum() < 1:
        a_df['cases'][k - 4:k + 1] = a_df['cases'][k - 4:k + 1].mean().round()
    elif a_df['cases'].iloc[k-3:k].sum() < 1:
        a_df['cases'][k - 3:k+1] = a_df['cases'][k - 3:k+1].mean().round()
    elif a_df['cases'].iloc[k-2:k].sum() < 1:
        a_df['cases'][k - 2:k+1] = a_df['cases'][k - 2:k+1].mean().round()

#Looking at our plot, something is off about April 21, 2021, there are 0 cases, and 3556 the next day. So we will average these two
a_df['cases']['2021-04-21':'2021-04-22'] = a_df['cases']['2021-04-21':'2021-04-22'].mean()


#Autocorrelation function
#Partial autocorrelation plot

#An ACF shows the correlation of the time series with itself, lagged behind by x time units
# so y axis is the correlation and x axis is the number of units of lag

#ACF and PACF give us parameters to use for arima models, but we can use a gridsearch for it.

import statsmodels.api as sm
from statsmodels.tsa.stattools import acovf, acf, pacf, pacf_yw, pacf_ols
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf

acf(a_df['cases'])

# pacf_yw(a_df['cases'], nlags=8, method='unbiased')
pacf_ols(a_df['cases'], nlags=10)

from pandas.plotting import lag_plot


#Very strong autocorrelation
lag_plot(a_df['cases'])

#A stationary dataset would not have this
plot_acf(a_df['cases'], lags = 40)
plot_pacf(a_df['cases'], lags = 40)

#PACF: If there is a sharp drop after a lag, use ARIMA, if there is a gradual decline, use an AR model
#ACF: to better identify if a MA model should be used, use ACF

#ARIMA models
# you need to stationary it
# p,d,q are needed for the ARIMA MODEL
# AR - p: A regression model that utilizes the depentent relationships between current observations and previous ones
# intergrated - d: Substracting an observation from a previous one in order to make it stationary
# MA - q: A model that uses the dependency between an observation and residual error from a moving average model applied to lagged observations

#Stationary has a constant mean and variance over time
#use a dickey-fuller test to see if the data is stationary
# def test_station(time_series, win_len):
#
#     #If time_series == panda series, then turn it into a df
#     if isinstance(time_series, pd.Series) == True:
#         time_series = time_series.to_frame()
#
#     # Determine rolling statisitics
#     movingmean = time_series.rolling(window=win_len).mean()
#     movingstd = time_series.rolling(window=win_len).std()
#
#     # plot rolling statistics with original
#     orig = plt.plot(time_series, color="blue", label="original")
#     mean = plt.plot(movingmean, color="red", label="rolling mean")
#     var = plt.plot(movingstd, color="green", label="rolling std")
#     plt.legend(loc="best")
#     plt.title("Rolling mean and std")
#     plt.show(block=False)
#
#     # Perform dickey fuller test for stationaririty
#     dickey_test = adfuller(time_series[time_series.columns[0]], autolag="AIC")
#     print(dickey_test)
#     print('ADF Statistic: {}'.format(dickey_test[0]))
#     print('p-value: {}'.format(dickey_test[1]))
#     print('Critical Values:')
#     for key, value in dickey_test[4].items():
#         print('\t{}: {}'.format(key, value))
#
# test_station(a_df['cases'], win_len=7)

# p < 0.05, meaning IT IS STATIONARY!!! WE DO NOT NEED TO MAKE IT STATIONARY
#
# #Monthly plots for seasonaility
# from statsmodels.graphics.tsaplots import quarter_plot, month_plot
#
# b_df = a_df
#
# month_plot(a_df.resample('Q-DEC').sum())


from statsmodels.tsa.ar_model import AR, ARResults
from statsmodels.tsa.arima.model import ARIMA
from pmdarima import auto_arima
from statsmodels.tsa.statespace.sarimax import SARIMAX
from sklearn.model_selection import GridSearchCV
import itertools
from sklearn.metrics import mean_squared_error

#Train, test, split
#Train test split
num_pred = 14

train = a_df['cases'][:-num_pred]
test = a_df['cases'].tail(num_pred)

#Right now, we don't worry about train/test/split, we just want to see what the AIC for the best model is, then we can do our forecasting
fit_models = auto_arima(a_df['cases'], seasonal=True, m = 7)
fit_models.summary()

start = len(train)
end=len(train) + len(test) - 1

#SARIMAX(3, 1, 2)x(2, 0, 2, 7)

mod = SARIMAX(train, order=(3,1,2), seasonal_order=(2, 0, 2, 7)).fit()
preds = mod.predict(start, end)
# print('ARIMA{}x{}12 - AIC:{}'.format(param, param_seasonal))
# print(f'AIC:{preds.aic}')
print(f'RMSE: {np.sqrt(mean_squared_error(test, preds))}')


final_model = SARIMAX(a_df['cases'], order=(3,1,2), seasonal_order=(2, 0, 2, 7)).fit()

final_model.forecast(7)

