import pandas as pd
import numpy as np
import os
import requests
import math
import io
import matplotlib.pylab as plt
import warnings
from statsmodels.tsa.stattools import adfuller

#Change directory to our main local computer documents
os.chdir('/home/jeff/Documents/COVID_19_time_series_predictions/')


#import pandas dataframe for cases in by provinces from the github repo for covid19 data set


#import pandas dataframe for cases in by provinces from the github repo for covid19 data set
urls = ['https://github.com/ccodwg/Covid19Canada/blob/master/timeseries_prov/testing_timeseries_prov.csv',
        'https://github.com/ccodwg/Covid19Canada/blob/master/timeseries_prov/vaccine_administration_timeseries_prov.csv',
        'https://github.com/ccodwg/Covid19Canada/blob/master/timeseries_prov/active_timeseries_prov.csv',
        'https://github.com/ccodwg/Covid19Canada/blob/master/timeseries_prov/mortality_timeseries_prov.csv',
        'https://github.com/ccodwg/Covid19Canada/blob/master/timeseries_prov/recovered_timeseries_prov.csv']

url = 'https://raw.githubusercontent.com/ishaberry/Covid19Canada/master/timeseries_prov/cases_timeseries_prov.csv'
s=requests.get(url).content
df_new=pd.read_csv(io.StringIO(s.decode('utf-8')),parse_dates=True)
df_new = df_new[df_new['province'] == 'Alberta'][['date_report', 'cases']]

for url in urls:
    print(url)
    s = requests.get(url).content
    df = pd.read_html(io.StringIO(s.decode('utf-8')),parse_dates=True)[0]
    df.rename(columns={df.filter(regex='date').columns[0]: 'date_report'}, inplace=True)
    df = df[df['province'] == 'Alberta']
    df_new = df_new.merge(df, how = 'outer', on = 'date_report')


df_new = df_new.loc[:, ~df_new.columns.str.contains('^Unnamed')]
df_new = df_new.loc[:, ~df_new.columns.str.contains('^province')]
df_new = df_new.loc[:, ~df_new.columns.str.contains('^cumulative')]
df_new.drop(['testing_info'], axis=1, inplace=True)



#This gives you an actual datetime index instead of a string
df_new['date_report'] = pd.to_datetime(df_new['date_report'], dayfirst=True)
df_new['weekend'] = df_new['date_report'].dt.dayofweek
# df_new['date_report'] = df_new['date_report'].dt.strftime('%m-%d-%Y')
# df_new['date_report'] = df_new['date_report'].apply(lambda x: x.strftime('%d-%m-%Y'))


#Set date index as your index. This should make things easier for plotting and such
df_new = df_new.set_index(['date_report'])
df_new.index = pd.to_datetime(df_new.index)

#Remove up until start of pandemic (April 3 really)
df_new = df_new.drop(pd.date_range('2020-01-25', '2020-03-05'), errors='ignore')


#here, we will fille the weekend 0 numbers with the average from the monday or tuesday depending on holidays
for k in list(np.where((df_new['weekend'] == 0) | (df_new['weekend'] == 1)))[0][3:]:
    for col in df_new.columns[:-1]:
        if df_new[col].iloc[k - 4:k].isna().any() == True:
            pass
        else:
            if df_new[col].iloc[k - 4:k].sum() < 1:
                df_new[col][k - 4:k + 1] = df_new[col][k - 4:k + 1].mean().round()
            elif df_new[col].iloc[k-3:k].sum() < 1:
                df_new[col][k - 3:k+1] = df_new[col][k - 3:k+1].mean().round()
            elif df_new[col].iloc[k-2:k].sum() < 1:
                df_new[col][k - 2:k+1] = df_new[col][k - 2:k+1].mean().round()

#Looking at our plot, something is off about April 21, 2021, there are 0 cases, and 3556 the next day. So we will average these two
df_new['cases']['2021-04-21':'2021-04-22'] = df_new['cases']['2021-04-21':'2021-04-22'].mean()

#turn all columns into integers

df_new = df_new['2020-12-14':]

# cols = ['cases', 'testing', 'avaccine', 'active_cases_change', 'deaths', 'recovered']
#
# for column in cols:
#     df_new[column] = df_new[column].astype('Int64')

#Train, test, split
#Train test split
num_pred = 7

train = df_new.iloc[:-num_pred]
test = df_new.tail(num_pred)

start = len(train)
end = len(train) + len(test) - 1

from pmdarima import auto_arima
from statsmodels.tsa.statespace.sarimax import SARIMAX
from sklearn.model_selection import GridSearchCV
import itertools
from sklearn.metrics import mean_squared_error

auto_arima(df_new['cases'], exogenous = df_new[['testing', 'avaccine', 'active_cases', 'deaths', 'recovered']], seasonal=True, m = 7, enforce_invertibility=False).summary()

# SARIMAX(0, 1, 1) x(1, 0, [], 7)

mod = SARIMAX(train['cases'], exog=train[['testing', 'avaccine', 'active_cases', 'deaths', 'recovered']], order=(0,1,1), seasonal_order=(1, 0, 0, 7), enforce_invertibility=False).fit()
preds = mod.predict(start, end, exog=test[['testing', 'avaccine', 'active_cases', 'deaths', 'recovered']].astype(int))
# print('ARIMA{}x{}12 - AIC:{}'.format(param, param_seasonal))
# print(f'AIC:{preds.aic}')

test_preds = test['cases']
print(f'RMSE: {np.sqrt(mean_squared_error(test_preds, preds))}')


final_model = SARIMAX(df_new['cases'].head(-7), exog=df_new[['testing', 'avaccine', 'active_cases', 'deaths', 'recovered']].head(-7), order=(0,1,1), seasonal_order=(1, 0, 0, 7), enforce_invertibility=False).fit()

final_preds = final_model.predict(len(df_new['cases'].head(-7)), len(df_new['cases'].head(-7)) + 7,  exog=df_new[['testing', 'avaccine', 'active_cases', 'deaths', 'recovered']].tail(8))

np.sqrt(mean_squared_error(df_new['cases'].tail(7), final_model.forecast(7, exog= df_new[['testing', 'avaccine', 'active_cases', 'deaths', 'recovered']].tail(7))))
